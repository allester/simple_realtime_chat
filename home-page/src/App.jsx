import React from "react";
import ReactDOM from "react-dom";

// Bootstrap CSS
import "bootstrap/dist/css/bootstrap.min.css"

import "./index.css";

import Chat from "chat/Chat";

const App = () => (
    <>
        <div className="container">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sagittis gravida purus ut aliquet. Cras sit amet risus condimentum, pretium mauris eget, congue felis. Aenean in lacus nisl. Proin id libero eleifend, varius est non, lacinia enim. In elit augue, congue a hendrerit sed, scelerisque nec diam. Ut sit amet turpis nec dolor aliquet scelerisque sed in libero. Fusce dapibus nunc libero, in faucibus libero tristique tincidunt.</p>
            <h1>Chat!</h1>
            <div>
                <Chat />
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sagittis gravida purus ut aliquet. Cras sit amet risus condimentum, pretium mauris eget, congue felis. Aenean in lacus nisl. Proin id libero eleifend, varius est non, lacinia enim. In elit augue, congue a hendrerit sed, scelerisque nec diam. Ut sit amet turpis nec dolor aliquet scelerisque sed in libero. Fusce dapibus nunc libero, in faucibus libero tristique tincidunt.</p>
        </div>
    </>
);

ReactDOM.render(<App />, document.getElementById("app"));
