import React from "react";
import ReactDOM from "react-dom";

// Bootstrap CSS
import "bootstrap/dist/css/bootstrap.min.css"

// Bootstrap Bundle JS
// import "bootstrap/dist/js/bootstrap.bundle.min"

import "./index.css";
import Chat from "./Chat";

const App = () => <Chat />;

ReactDOM.render(<App />, document.getElementById("app"));
