# Real-time Chat System

A simple real time chat system built with GraphQL subscriptions.

This application utilises Webpack module federation to effectively access and utilise the chat system within another simple website/ application.

## Setup

### Install packages/ dependencies

Clone the repository to your local environment

#### Server Setup

cd into `server` directory

```bash
npm install
```

#### Chat Application Setup

cd into `client` directory

```bash
npm install
```

#### Simple Website Setup

cd into `home-page` directory

```bash
npm install
```

### Start Server

cd into `server` directory

```bash
npm start
```

### Start Chat Application

cd into `client` directory

```bash
npm start
```

### Start Simple Website

cd into `home-page` directory

```bash
npm start
```